from torch.autograd import Variable
from PIL import Image
import numpy as np
import cv2
from torchvision import transforms
import torchvision.models as models
import torch
import matplotlib.pyplot as plt

#reading file that has names of each class
f = open("../Users/Mohamed/PycharmProjects/ML-Project/labels0.txt", 'r')
dic = eval(f.read()) #the eval() function runs the python code (which is passed as an argument)
f.close()

# Load the pretrained model
model = models.googlenet(pretrained=True) #can be replaced
# Set model to evaluation mode
model.eval()


#top_1 is the array that will hold the all time score of each class
top_1 = np.empty(1000)

# Configure VideoCapture class instance for using camera
cap = cv2.VideoCapture(0)

#normalize input for processing
scaler = transforms.Resize((224, 224))#the model expects images to be at least 224x224
normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],std=[0.229, 0.224, 0.225])


#feeding the captured frame to the model
def analyze(frame):
    img = Image.fromarray(frame)
    to_tensor = transforms.ToTensor()

    #create a PyTorch Variable with the transformed image
    t_img = Variable(normalize(to_tensor(scaler(img))).unsqueeze(0))
    #Run the model on our transformed image
    my_embedding = model(t_img)
    return my_embedding

# Initialize plot.
fig, ax = plt.subplots()

# Initialize plot line object(s). Turn on interactive plotting and show plot.
lw = 1
plt.ion()
plt.show()

count = 0
# Grab, process, and display video frames. Update plot line object(s).
while True:

    (grabbed, frame) = cap.read() # frame is a 2D numpy array

    if not grabbed:
        break
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    l=analyze(gray)
    # Use the model object to select the desired layer
    t = torch.nn.functional.softmax(l[0], dim=0)
    t = t.detach().numpy()
    top_5 = t.argsort()[-5:][::-1]
    top_1[top_5[0]] += 1
    X = [dic[e] for e in top_5]
    Y = [t[e] for e in top_5]

    cv2.imshow('Camera live', gray)
    ax.set_title('Histogram of top 5 estimated classes')
    ax.set_ylabel('Probability')
    lineGray, = ax.plot(X, Y, c='k', lw=lw)
    lineGray.set_xdata(X)
    lineGray.set_ydata(Y)
    # draw histogram chart
    plt.bar(range(len(top_5)), Y, align='center',color='black')
    plt.xticks(range(len(top_5)), X, fontsize=6)
    fig.canvas.draw()

    #CLEAN LAST CHART
    plt.cla()
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

cap.release()
cv2.destroyAllWindows()