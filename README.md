# ML-Project

Demo tool with Python that acquires images live from the notebook camera and uses a pre-trained ImageNet model (from Torchvision) to classify objects depicted in such images.